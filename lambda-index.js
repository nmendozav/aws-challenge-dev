const http = require("https");

function eliminarDiacriticos(texto) {
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
}

const getData =(city)=>{
    return new Promise((resolve, reject)=>{
        try{
            const QUERY = eliminarDiacriticos(city).replace(/^$|\s+/gi, "%20");
            console.log(QUERY);
            const options = {
            	"method": "GET",
            	"hostname": "community-open-weather-map.p.rapidapi.com",
            	"port": null,
            	"path": `/forecast/daily?q=${QUERY}&lat=0&lon=0&id=2172797&lang=null&units=%22metric%22%20or%20%22imperial%22&mode=xml%2C%20html`,
            	"headers": {
            		"x-rapidapi-key": "0423aa8051msh2e8d8d90e7c96edp1f2509jsn2b683433dce3",
            		"x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
            		"useQueryString": true,
            		 "content-type": "text/plain; charset=utf-8",
            	}
            };
            const req = http.request(options, function (res) {
        	const chunks = [];
        
        	res.on("data", function (chunk) {
        		chunks.push(chunk);
        	});
        
        	res.on("end", function () {
            		const body = Buffer.concat(chunks);
            		
            		console.log('body', body.toString());
            		resolve(JSON.parse(body.toString()));
            	});
            });
            
            req.on('error', function(e) {
                console.error('error');
                console.error(e);
                reject(e);
            });
            
            req.end();
        }catch(e){
            console.log(e);
            reject(e);
        }
    })
}

exports.handler = async (event) => {
    console.log("event", event);
    let response = {};
    await getData(event.text?event.text:'Santiago').then(res =>{
        response.statusCode = 200;
        response.body = res;
    }).catch(e=>{
        response.statusCode = 400;
        response.body = e;
    });
    return response;
};
